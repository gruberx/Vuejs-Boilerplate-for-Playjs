# Vuejs-Boilerplate-for-Playjs

> A full-featured Webpack setup with CSS loading, file loading, and Vue templates compiling.

> This template is Vue 2.0 compatible. 

## Usage

This is a basic starting template to run a Vue.js project. It will load files via WebPack, and build Vue templates. Great for working through examples while learning on the road on your iPad in Play.js

Create your first app on the iPad by tapping editor, select React as the project, checkmark the 'clone from Git' option, name your project, and tap Create. The next view will require you to paste this repos URL. Then tap clone to pull the repo into Play.js. Tap the play icon on the left hand toolbar to build, and load the project. Any changes to files will require you to tap stop, and restart the server. The development server will run on the PORT specified by Play.js. If you change the PORT in index.js Play.js will still use the default PORT when opening the browser.

### Running on an iOS device
If you're using [Play.js](https://playdotjs.com), just clone this repository and press to start it.

### Running on a computer
If you're using a Windows/Linux/Mac, to start the server you must first install the packages:

`npm install`

`npm run build`

Set the environment variable `'REACT_APP_PORT'` to `3000` (as in int), and then run:

`node index.js`

and access `http://localhost:3000`